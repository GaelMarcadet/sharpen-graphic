#include <opencv2/opencv.hpp>
#include <vector>
#include <math.h>
#include <IL/il.h>
#include <cuda.h>
#include <string>

typedef unsigned char uchar;

using namespace std;


__global__ void sharpen( unsigned char * in, unsigned char * out, std::size_t cols, std::size_t rows ) {
    auto li = threadIdx.x;
    auto lj = threadIdx.y;
    auto i = blockIdx.x * ( blockDim.x - 2 ) + threadIdx.x;
    auto j = blockIdx.y * ( blockDim.y - 2 ) + threadIdx.y;

    extern __shared__ unsigned char sdata[];

    
    sdata[ lj * blockDim.x + li ] = 
    (
          307 * in[ 3 * ( j * cols + i )     ]
        + 604 * in[ 3 * ( j * cols + i ) + 1 ]
        + 113 * in[ 3 * ( j * cols + i ) + 2 ]
    ) / 1024;

    __syncthreads();

    if ( 0 < li && li < blockDim.x - 1 && 0 < lj && lj < blockDim.y - 1 ) {
        out[ j * cols + i ] = 
            sdata[ lj * blockDim.x + li ] +
            ( 
                sdata[ lj * blockDim.x + li ] - 
                (
                    sdata[ ( lj - 1 ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj - 1 ) * blockDim.x + li     ] +
                    sdata[ ( lj - 1 ) * blockDim.x + li + 1 ] +

                    sdata[ ( lj     ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj     ) * blockDim.x + li     ] +
                    sdata[ ( lj     ) * blockDim.x + li + 1 ] +

                    sdata[ ( lj + 1 ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj + 1 ) * blockDim.x + li     ] +
                    sdata[ ( lj + 1 ) * blockDim.x + li + 1 ] 
                ) / 9
            );
    }   
}



void check_errors( int checkStatus ) {
    auto status = cudaGetLastError();
    if ( status != cudaSuccess ) {
        std::cout << "Cuda error: " << checkStatus << ": " << cudaGetErrorString( status ) << std::endl;
        exit( status );
    }
}

void check_errors() {
    check_errors( 0 );
}


int main( int argc, char **args )
{
    // the program awaits as input the program name, and block dimensions
    if ( argc != 6 ) {
        cerr << "Usage: ./sharpen <blockDim.x : int> <blockDim.y : int> <nstreams : int> <infile: string> <outfile: string>" << endl;
        exit( 1 );
    }

    // parse arguements and checks semantics integrity
    dim3 grid;
    grid.x = atoi( args[ 1 ] );
    grid.y = atoi( args[ 2 ] );
    int nstreams = atoi( args[ 3 ] );

    char *infile = args[ 4 ];
    char *outfile = args[ 5 ];



    //cout << "Reading file " << infile << endl;
    cv::Mat m_in = cv::imread( infile, cv::IMREAD_UNCHANGED );

    auto rgb = m_in.data;
    auto rows = m_in.rows;
    auto cols = m_in.cols;

    // split image in size
    size_t stream_nrows[ nstreams ];
    auto rows_per_stream = rows / nstreams;
    auto remain_rows = rows % nstreams;
    for ( int i = 0; i < nstreams; i++ ) {
        if ( i < remain_rows ) {
            stream_nrows[ i ] = rows_per_stream + 1;
        } else {
            stream_nrows[ i ] = rows_per_stream;
        }
    }


    // creates rgb 
    uchar * rgb_d = nullptr;
    cudaMalloc( &rgb_d, 3 * cols * rows );
    check_errors();

    uchar * out_d = nullptr;
    cudaMalloc( &out_d, cols * rows );
    check_errors();

    // first we define the output in host
    uchar * out = nullptr;
    cudaMallocHost( &out, rows * cols );
    cv::Mat m_out( rows, cols, CV_8UC1, out );


    // constuct array of pointers on rgb, rgb_d, out, out_d
    uchar *rgb_array[ nstreams ];
    uchar *rgb_d_array[ nstreams ];
    uchar *out_array[ nstreams ];
    uchar *out_d_array[ nstreams ];

    auto rgb_pixel_size = 3;
    auto offset = 0;
    for ( int i = 0; i < nstreams; ++i ) {
        rgb_array[ i ] = rgb + offset;
        rgb_d_array[ i ] = rgb_d + offset;
        offset = offset + ( stream_nrows[ i ] - 2 * i ) * cols * rgb_pixel_size;
    }

    auto out_pixel_size = 1;
    offset = 0;
    for ( int i = 0; i < nstreams; ++i ) {
        out_array[ i ] = out + offset;
        out_d_array[ i ] = out_d + offset;
        offset = offset + ( stream_nrows[ i ] - 2 * i ) * cols * out_pixel_size;
    }


    // creates streams
    cudaStream_t streams[ nstreams ];
    for ( int i = 0; i < nstreams; ++i ) {
        cudaStreamCreate( &streams[ i ] );
        check_errors();
    }

    for ( int i = 0; i < nstreams; ++i ) {
        cudaMemcpyAsync( rgb_d_array[ i ], rgb_array[ i ], 3 * cols * stream_nrows[ i ], cudaMemcpyHostToDevice, streams[ i ] );
        check_errors( 1 );
    }


    // prepares cuda events to compute elapsed time for filter computation
    cudaEvent_t start, stop;
    cudaEventCreate( &start );
    cudaEventCreate( &stop );

    cudaEventRecord( start );

    for ( int i = 0; i < nstreams; i++ ) {
        // call the kernel with the first part of the image
        auto bx = ( cols - 1 ) / ( grid.x - 2 ) + 1;
        auto by = ( stream_nrows[ i ] - 1 ) / ( grid.y - 2 ) + 1;
        dim3 block( bx, by );
        sharpen<<< block, grid, grid.x * grid.y, streams[ i ] >>>( rgb_d_array[ i ], out_d_array[ i ], cols, stream_nrows[ i ] );
        check_errors( 2 );
    }


    for ( int i = 0; i < nstreams; ++i ) {
        cudaMemcpyAsync( out_array[ i ], out_d_array[ i ], cols * stream_nrows[ i ], cudaMemcpyDeviceToHost, streams[ i ] );
        check_errors( 3 );
    }

    // synchronize device to fill computed data
    cudaDeviceSynchronize();

    // get current time to compute elapsed time
    cudaEventRecord( stop );
    cudaEventSynchronize( stop );

    float elapsedTime;
    cudaEventElapsedTime( & elapsedTime, start, stop );

    // display elapsed time
    cout << "Total GPU computation time: " << elapsedTime << "ms with grid dimensions " << grid.x << " " << grid.y << " with " << nstreams << " streams" << endl;;



    cv::imwrite( outfile, m_out );
	
    cudaFree( rgb_d );
    cudaFree( out_d );
    

    return 0;
    
}

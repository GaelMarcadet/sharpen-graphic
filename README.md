# Projet de programmation graphique CUDA
Le projet présenté est une implémentation du filter *sharpen*.
Il a été réalisé par *R. LeCompte* et *G. Marcadet*, en C++/CUDA.

Ce projet contient plusieurs versions de ce filtre, qui à partir du image
en génére une nouvelle avec le filtre appliqué.

## Architecture du projet
- bench: Contient les benchs générés pas le fichier ./benchmark.sh
- build: Contient les exécutables du projet
- src: Contient les sources du projet
- res: Contient les ressources du projet et notamment les images.

### Versions du filtre
On retrouve dans ce projet 5 versions différentes du filtre sharpen.
- src/cpu: Le filtre pour cpu
- src/sharpen-v1: Le filtre pour GPU simple
- src/sharpen-v2: Le filtre pour GPU avec mémoire partagée
- src/sharpen-v3: Même chose que pour la v3 mais améliorée et rendu plus adaptée pour la mémoire shared.
- src/sharpen-v4: Le filtre GPU avec la mémoire partagée et les streams.

## Réaliser les benchmarks
Pour lancer les benchmarks, il suffit d'entrer la commande `make benchmark` à la racine du projet.
Chaque version sera alors compilée puis exécutée. 
Les meilleurs temps sont disponibles dans le fichier `bench/bench_file.txt`.

## Nettoyer le projet 
Pour nettoyer le projet, il suffit d'entrer la commande `make clean` à la racine du projet.
#include <opencv2/opencv.hpp>
#include <vector>
#include <math.h>
#include <IL/il.h>
#include <cuda.h>
#include <string>

using namespace std;

__global__ void sharpen( unsigned char * in, unsigned char * out, std::size_t cols, std::size_t rows ) {
    auto li = threadIdx.x;
    auto lj = threadIdx.y;
    auto i = blockIdx.x * ( blockDim.x - 2 ) + threadIdx.x;
    auto j = blockIdx.y * ( blockDim.y - 2 ) + threadIdx.y;

    extern __shared__ unsigned char sdata[];

    
    sdata[ lj * blockDim.x + li ] = 
    (
          307 * in[ 3 * ( j * cols + i )     ]
        + 604 * in[ 3 * ( j * cols + i ) + 1 ]
        + 113 * in[ 3 * ( j * cols + i ) + 2 ]
    ) / 1024;

    __syncthreads();

    if ( 0 < li && li < blockDim.x - 1 && 0 < lj && lj < blockDim.y - 1 ) {
        out[ j * cols + i ] = 
            sdata[ lj * blockDim.x + li ] +
            ( 
                sdata[ lj * blockDim.x + li ] - 
                (
                    sdata[ ( lj - 1 ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj - 1 ) * blockDim.x + li     ] +
                    sdata[ ( lj - 1 ) * blockDim.x + li + 1 ] +

                    sdata[ ( lj     ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj     ) * blockDim.x + li     ] +
                    sdata[ ( lj     ) * blockDim.x + li + 1 ] +

                    sdata[ ( lj + 1 ) * blockDim.x + li - 1 ] +
                    sdata[ ( lj + 1 ) * blockDim.x + li     ] +
                    sdata[ ( lj + 1 ) * blockDim.x + li + 1 ] 
                ) / 9
            );
    }   
}



void check_errors( int checkStatus ) {
    auto status = cudaGetLastError();
    if ( status != cudaSuccess ) {
        std::cout << "Cuda error: " << checkStatus << ": " << cudaGetErrorString( status ) << std::endl;
        exit( status );
    }
}

void check_errors() {
    check_errors( 0 );
}





int main( int argc, char **args )
{
    // the program awaits as input the program name, and block dimensions
    if ( argc != 6 ) {
        cerr << "Usage: ./sharpen <blockDim.x : int> <blockDim.y : int> <nstreams : int> <infile: string> <outfile: string>" << endl;
        exit( 1 );
    }

    // parse arguements and checks semantics integrity
    dim3 grid;
    grid.x = atoi( args[ 1 ] );
    grid.y = atoi( args[ 2 ] );
    int nstreams = atoi( args[ 3 ] );

    char *infile = args[ 4 ];
    char *outfile = args[ 5 ];



    //cout << "Reading file " << infile << endl;
    cv::Mat m_in = cv::imread( infile, cv::IMREAD_UNCHANGED );

    auto rgb = m_in.data;
    auto rows = m_in.rows;
    size_t cols = m_in.cols;

    size_t stream_nrows = rows / nstreams;

    

    // first we define the output in host
    unsigned char * out = nullptr;
    cudaMallocHost( &out, rows * cols );
    cv::Mat m_out( rows, cols, CV_8UC1, out );


    // then we allocates memory space in device for input and output
    // cudaSetDeviceFlags(cudaDeviceMapHost);
    // cudaHostRegister( rgb, 3 * rows * cols, cudaHostRegisterMapped );
    
    unsigned char * rgb_d = nullptr;
    cudaMalloc( &rgb_d, 3 * cols * rows );
    check_errors();

    unsigned char * out_d = nullptr;
    cudaMalloc( &out_d, cols * rows );
    check_errors();


    

    // creates streams
    cudaStream_t streams[ nstreams ];
    for ( int i = 0; i < nstreams; ++i ) {
        cudaStreamCreate( &streams[ i ] );
        check_errors();
    }


    for ( int i = 0; i < nstreams; ++i ) {
        cudaMemcpyAsync( rgb_d + i * ( 3 * cols * stream_nrows ), rgb + i * ( 3 * cols * stream_nrows ), 3 * cols * stream_nrows, cudaMemcpyHostToDevice, streams[ i ] );
        check_errors( 1 );
    }


    // prepares cuda events to compute elapsed time for filter computation
    cudaEvent_t start, stop;
    cudaEventCreate( &start );
    cudaEventCreate( &stop );
  
    cudaEventRecord( start );
    
    for ( int i = 0; i < nstreams; i++ ) {
        // call the kernel with the first part of the image
        auto bx = ( cols - 1 ) / ( grid.x - 2 ) + 1;
        auto by = ( stream_nrows - 1 ) / ( grid.y - 2 ) + 1;
        dim3 block( bx, by );
        sharpen<<< block, grid, grid.x * grid.y, streams[ i ] >>>( rgb_d + i * ( 3 * cols * stream_nrows ), out_d + i * ( cols * stream_nrows ), cols, stream_nrows );
        check_errors( 2 );
    }


    for ( int i = 0; i < nstreams; ++i ) {
        cudaMemcpyAsync( out + i * ( cols * stream_nrows ), out_d + i * ( cols * stream_nrows ), cols * stream_nrows, cudaMemcpyDeviceToHost, streams[ i ] );
        check_errors( 3 );
    }

    // synchronize device to fill computed data
    cudaDeviceSynchronize();

    // get current time to compute elapsed time
    cudaEventRecord( stop );
    cudaEventSynchronize( stop );

    float elapsedTime;
    cudaEventElapsedTime( & elapsedTime, start, stop );

    // exporting file
    // cout << "Exporting output file as " << outfile << endl; 
    #ifdef GEN_OUT
    cv::imwrite( outfile, m_out );
    #endif


    // display elapsed time
    cout << "Total GPU computation time: " << elapsedTime << "ms with grid dimensions " << grid.x << " " << grid.y << " with " << nstreams << " streams" << endl;;
	
    cudaFree( rgb_d );
    cudaFree( out_d );
	
    return 0;
}

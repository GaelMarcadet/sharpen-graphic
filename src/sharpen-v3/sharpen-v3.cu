#include <opencv2/opencv.hpp>
#include <vector>

__global__ void sharpen( unsigned char * in, unsigned char * out, std::size_t cols, std::size_t rows ) {
  auto li = threadIdx.x;
  auto lj = threadIdx.y;
  auto i = blockIdx.x * ( blockDim.x - 2 ) + threadIdx.x;
  auto j = blockIdx.y * ( blockDim.y - 2 ) + threadIdx.y;

  extern __shared__ unsigned char sdata[];

  
  sdata[ lj * blockDim.x + li ] = 
  (
        307 * in[ 3 * ( j * cols + i )     ]
      + 604 * in[ 3 * ( j * cols + i ) + 1 ]
      + 113 * in[ 3 * ( j * cols + i ) + 2 ]
  ) / 1024;

  __syncthreads();

  if ( 0 < li && li < blockDim.x - 1 && 0 < lj && lj < blockDim.y - 1 ) {
      out[ j * cols + i ] = 
          sdata[ lj * blockDim.x + li ] +
          ( 
              sdata[ lj * blockDim.x + li ] - 
              (
                  sdata[ ( lj - 1 ) * blockDim.x + li - 1 ] +
                  sdata[ ( lj - 1 ) * blockDim.x + li     ] +
                  sdata[ ( lj - 1 ) * blockDim.x + li + 1 ] +

                  sdata[ ( lj     ) * blockDim.x + li - 1 ] +
                  sdata[ ( lj     ) * blockDim.x + li     ] +
                  sdata[ ( lj     ) * blockDim.x + li + 1 ] +

                  sdata[ ( lj + 1 ) * blockDim.x + li - 1 ] +
                  sdata[ ( lj + 1 ) * blockDim.x + li     ] +
                  sdata[ ( lj + 1 ) * blockDim.x + li + 1 ] 
              ) / 9
          );
  }   
}


int main( int argc, char **args )
{

  using namespace std;
  // we need 4 arguments: input output blockDim.x blockDimx.y
  if ( argc != 5 ) {
    cout << "Invalid number of argument " << endl;
    cout << "Usage: <blockDim.x : int> <blockDim.y : int> <input : str> <ouput : str>" << endl;
    exit( 1 );
  }
  int block_dim_x = atoi( args[ 1 ] );
  int block_dim_y = atoi( args[ 2 ] );
  char *input_filename = args[ 3 ];
  char *ouput_filename = args[ 4 ];


  cv::Mat m_in = cv::imread( input_filename, cv::IMREAD_UNCHANGED );
  auto rgb = m_in.data;
  auto rows = m_in.rows;
  auto cols = m_in.cols;
  std::vector< unsigned char > g( rows * cols );
  cv::Mat m_out( rows, cols, CV_8UC1, g.data() );
  unsigned char * rgb_d;
  unsigned char * out_d;

  cudaMalloc( &rgb_d, 3 * rows * cols );
  cudaMalloc( &out_d, rows * cols );
  cudaMemcpy( rgb_d, rgb, 3 * rows * cols, cudaMemcpyHostToDevice );
  
  dim3 t( block_dim_x, block_dim_y );
  dim3 b( ( cols - 1) / t.x + 1 , ( rows - 1 ) / t.y + 1 );

  // prepares cuda events to compute elapsed time for filter computation
  cudaEvent_t start, stop;
  cudaEventCreate( &start );
  cudaEventCreate( &stop );
  cudaEventRecord( start );

  sharpen<<< b, t, t.x * t.y >>>( rgb_d, out_d, cols, rows );

  // get current time to compute elapsed time
  cudaEventRecord( stop );
  cudaEventSynchronize( stop );

  float elapsedTime;
  cudaEventElapsedTime( & elapsedTime, start, stop );
  // display elapsed time
  cout << "Total GPU computation time: " << elapsedTime << "ms with grid dimensions " << t.x << " " << t.y <<endl;;


  cudaDeviceSynchronize();
  auto err = cudaGetLastError();
  if( err != cudaSuccess )
  {
    std::cout <<"Cuda error "<< cudaGetErrorString( err );
  }

  cudaMemcpy( g.data(), out_d, rows * cols, cudaMemcpyDeviceToHost );
  cv::imwrite( ouput_filename, m_out );
  cudaFree( rgb_d );
  cudaFree( out_d );
  return 0;
}

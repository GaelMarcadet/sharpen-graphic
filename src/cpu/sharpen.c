#include <stdio.h>
#include <stdlib.h>


#include <sys/time.h>

#include <math.h>

#include <IL/il.h>

int main( int agrc, char **args )
{
    if ( agrc != 3 ) {
        printf( "Usage %s <in_file: str> <out_file : str>", args[ 0 ] );
        exit( EXIT_FAILURE );
    }


    char *infile = args[ 1 ];
    char *outfile = args[ 2 ];

    unsigned int image;

    ilInit();

    ilGenImages(1, &image);
    ilBindImage(image);
    ilLoadImage( infile );

    int width, height, bpp, format;

    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
    format = ilGetInteger(IL_IMAGE_FORMAT);

    // Récupération des données de l'image
    unsigned char *data = ilGetData();

    // Traitement de l'image
    unsigned char *out = (unsigned char *)malloc(width * height * bpp);

    int h, v, res;

    struct timeval start, stop;

    gettimeofday(&start, 0);

    for ( int j = 1; j < height - 1; j++ ) {
        for ( int i = 1; i < width - 1; i++ ) {
            unsigned char details[ 3 ];
            for ( int c = 0; c < 3; c++ ) {
                details[ c ] = 
                data[ 3 * ( j * width + i ) + c ] -
                (
                    data[ 3 * ( ( j - 1 ) * width + i - 1 ) + c ] +
                    data[ 3 * ( ( j - 1 ) * width + i     ) + c ] +
                    data[ 3 * ( ( j - 1 ) * width + i + 1 ) + c ] +

                    data[ 3 * ( ( j     ) * width + i - 1 ) + c ] +
                    data[ 3 * ( ( j     ) * width + i     ) + c ] +
                    data[ 3 * ( ( j     ) * width + i + 1 ) + c ] +

                    data[ 3 * ( ( j + 1 ) * width + i - 1 ) + c ] +
                    data[ 3 * ( ( j + 1 ) * width + i     ) + c ] +
                    data[ 3 * ( ( j + 1 ) * width + i + 1 ) + c ]
                ) / 9;   
            }

            unsigned char detail = ( details[ 0 ] + details[ 1 ] + details[ 2 ] ) / 3;
            details[ 0 ] = details[ 1 ] = details[ 2 ] = detail;

            for ( int c = 0; c < 3; ++c ) {
                out[ 3 * ( j * width + i ) + c ] = data[ 3 * ( j * width + i ) + c ] + detail;
            } 
        }
    }

    gettimeofday(&stop, 0);

    printf("time %li\n", ( stop.tv_usec) - (  start.tv_usec));

    //Placement des données dans l'image
    ilSetData(out);

    // Sauvegarde de l'image
    ilEnable(IL_FILE_OVERWRITE);
    ilSaveImage( outfile );
    ilDeleteImages(1, &image);

    free(out);
}